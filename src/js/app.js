import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const ul = document.querySelector("ul");

  showPokemons(ul);
});

function showPokemons(ul) {
  getPokemons()
  .then(pokemons => {
    pokemons.forEach(pokemon => {
      let li = document.createElement("li");
      li.innerText = pokemon.name;
      ul.appendChild(li);
    });
  });
}

function getPokemons() {
  return fetch(`https://pokeapi.co/api/v2/pokemon?limit=10`)
  .then(checkStatus)
  .then(toJSON)
  .then(data => {
    return data.results;
  })
  .catch(err => {
    console.log(err);
    return [];
  })
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response);
  }

  return Promise.reject(response);
}

function toJSON(response) {
  return response.json();
}